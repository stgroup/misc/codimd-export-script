from export_md.codimd_export import export_from_codimd
from import_md.hedgedoc_import import import_into_hedgedoc
from common import get_sessionid, print_block_heading

if __name__ == "__main__":
    export_folder = "codimd-documents"
    export_archive = "archive.zip"

    print_block_heading("Beginning export from CodiMD...")
    export_from_codimd("https://md.inf.tu-dresden.de", get_sessionid("CodiMD", "connect.sid"), export_folder)

    print_block_heading("Beginning import to HedgeDoc...")
    import_into_hedgedoc("https://md.inf.tu-dresden.de/notes", get_sessionid("HedgeDoc", "connect.hedgeDoc.sid"),
                         export_folder, export_archive)
