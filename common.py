import json
import sys
import urllib.request
from urllib.error import HTTPError

if not sys.platform.startswith("win"):
    from getpass import getpass
else:
    # on Windows, Ctrl-V doesn't work with getpass()
    getpass = input


def get_sessionid(service_name, cookie_key):
    """Ask the user for the session id, if it's not configured as an envvar."""
    print(f"You now will be asked for your session cookie for {service_name}")
    print(f"To extract this cookie, you need to open your browser with an active and logged-in {service_name} session")
    print(f"In Firefox or Chrome, you can do this by following the following steps:")
    print(f"\t1. Open the developer tools (Shortcut: F12")
    print(f"\t2. Select \"Storage -> Cookies\" (Firefox) or \"Application -> Cookies\"")
    print(f"\t3. Copy the value for the cookie named {cookie_key}")
    print(f"\t4. Input it in the prompt")

    sid = getpass(f"Please provide your {service_name} session id ({cookie_key} cookie): ")
    if sid.startswith("s%3A"):
        return sid
    raise SystemExit(f"error: the supplied session id seems to be malformed")


def print_block_heading(message):
    separator = "======================================================================================================"
    print(separator)
    print(message)
    print(separator)


#def check_accessibility(instance_url, session_id, cookie_key):
#    request_url = instance_url + '/me/'
#    headers = {"Cookie": f"{cookie_key}={session_id}"}
#    try:
#        req = urllib.request.Request(request_url, headers=headers)
#        with urllib.request.urlopen(req) as response:
#            response_json = json.load(response)
#            if response_json["status"] != "ok":
#                raise_no_connection_error(request_url)
#        print(f"Could access protected resources at {instance_url}. Proceeding...")
#    except HTTPError as error:
#        print(f"HTTP {error.status} {error.reason}")
#        raise_no_connection_error(request_url)


def raise_no_connection_error(request_url):
    raise SystemExit(f"Could not access protected resources at {request_url}. Make sure that the specified "
                     f"cookie is correct. Aborting...")
