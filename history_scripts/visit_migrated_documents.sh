#!/usr/bin/env bash
open_command=open
if [[ $(command -v xdg-open) ]];
then
  open_command=xdg-open
fi
script_directory=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
url_file=$script_directory/hedgedoc_documents_to_visit.url
readarray -t url_array < "$url_file"
for i in "${url_array[@]}"
do
  eval "$open_command $i"
done